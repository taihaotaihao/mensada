/*
 * MIT License
 *
 * Copyright (c) 2022-2022.
 * Taihao Zhang (http://taihao.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.taihao.mensada.model

/**
 * Category of a meal.
 * @param emoji Emoji representing this category
 * @param ids ID as specified at the Stw's website
 */
enum class Category(val emoji: String, vararg val ids: Int) {
    VEGAN("ⓥ", 2),
    CARROT("\uD83E\uDD55", 3),
    ROOSTER("\uD83D\uDC13", 4),
    PIG("\uD83D\uDC16", 5),
    COW("\uD83D\uDC04", 6, 7),
    SHEEP("\uD83D\uDC11", 8),
    DEER("\uD83E\uDD8C", 9),
    FISH("\uD83D\uDC1F", 10);

    /**
     * Joins two categories to a string.
     */
    operator fun plus(other: Category) = "$emoji / ${other.emoji}"

    companion object {
        /**
         * Gets the emoji associated with the data-cat ID which is found
         * inside a meal's HTML code.
         * @param dataCatId ID from HTML, in 1..14
         * @return One emoji if ID in 2..10, joint emoji if ID == 1 or in 11..14,
         * empty string otherwise
         */
        fun getEmoji(dataCatId: Int): String {
            return when (dataCatId) {
                1 -> VEGAN + PIG // idk but they have this
                in 2..10 -> values().first { dataCatId in it.ids }.emoji
                11 -> PIG + ROOSTER
                12 -> PIG + FISH
                13 -> COW + PIG
                14 -> ROOSTER + FISH
                else -> ""
            }
        }
    }
}
