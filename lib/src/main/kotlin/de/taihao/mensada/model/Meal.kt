/*
 * MIT License
 *
 * Copyright (c) 2022-2022.
 * Taihao Zhang (http://taihao.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.taihao.mensada.model

/**
 * Data class representing a meal.
 * @property title Displayed title of the meal
 * @property allergens Allergen codes, if applicable
 * @property category Integer corresponding to the meal category
 */
data class Meal(
    val title: String,
    val allergens: String,
    val category: Int,
    val price: Price,
    val location: String
) : Comparable<Meal> {
    override fun compareTo(other: Meal): Int {
        return if (this === other) 0
        else
            Comparator.comparing(Meal::location)
                .thenComparing(Meal::category)
                .thenComparing(Comparator.comparing(Meal::price).reversed())
                .thenComparing(Meal::title)
                .thenComparing(Meal::allergens)
                .compare(this, other)
    }
}
