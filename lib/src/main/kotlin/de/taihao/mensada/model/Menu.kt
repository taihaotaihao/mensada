/*
 * MIT License
 *
 * Copyright (c) 2022-2022.
 * Taihao Zhang (http://taihao.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.taihao.mensada.model

import java.time.LocalDate

/**
 * A menu is an [ArrayList] of a given day's [Meal]s, accompanied by additional information that
 * identify a unique menu.
 * @property date Date for which this menu is valid
 * @property canteen Canteen for which this menu is valid
 */
data class Menu(
    val date: LocalDate,
    val canteen: Canteen
) : ArrayList<Meal>() {
    /**
     * Works like [Iterable.filter] but returns a [Menu] instead of a [List].
     * @param predicate Predicate to filter by
     * @return Menu with only meals matching the given predicate
     * */
    fun filter(predicate: (Meal) -> Boolean) = filterTo(Menu(date, canteen), predicate)

    /**
     * Works like [Iterable.filterNot] but returns a [Menu] instead of a [List].
     * @param predicate Predicate to filter by
     * @return Menu with only meals not matching the given predicate
     * */
    fun filterNot(predicate: (Meal) -> Boolean) = filter { !predicate(it) }

    override fun toString(): String {
        // Needed so toString prints the elements of this collection
        return super.toString()
    }
}
