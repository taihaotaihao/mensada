/*
 * MIT License
 *
 * Copyright (c) 2022-2022.
 * Taihao Zhang (http://taihao.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.taihao.mensada.model

import de.taihao.mensada.util.capitalise
import java.net.URL

/**
 * Canteen locations of the Stw.
 * @property url URL where the menu is located
 */
enum class Canteen(val url: URL) {
    STADTMITTE(URL("https://studierendenwerkdarmstadt.de/hochschulgastronomie/speisen/stadtmitte/")),
    LICHTWIESE(URL("https://studierendenwerkdarmstadt.de/hochschulgastronomie/speisen/lichtwiese/")),
    SCHOEFFERSTRASSE(URL("https://studierendenwerkdarmstadt.de/hochschulgastronomie/speisen/schoefferstrasse/")),
    DIEBURG(URL("https://studierendenwerkdarmstadt.de/hochschulgastronomie/speisen/dieburg/")),
    SCHOEFFERS(URL("https://studierendenwerkdarmstadt.de/hochschulgastronomie/speisen/schoeffers/"));

    override fun toString(): String {
        // Not the best solution but it works for now. Maybe an actual 'name' field could be useful
        return name.capitalise().replace("ss", "ß").replace("oe", "ö")
    }
}
