/*
 * MIT License
 *
 * Copyright (c) 2022-2022.
 * Taihao Zhang (http://taihao.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.taihao.mensada.model

import de.taihao.mensada.util.round

/**
 * Data class representing a price combination.
 * @property studentPrice Student's price
 * @property normalPrice Normal (non-student) price. Can be [Double.NaN]
 */
data class Price(val studentPrice: Double, val normalPrice: Double) : Comparable<Price> {
    init {
        // Ensure the Price object about to be constructed is valid
        if (!studentPrice.isFinite() && normalPrice.isFinite()) {
            throw IllegalStateException("Student price must be set in order to set normal price.")
        }
    }

    override fun toString(): String {
        var result = if (studentPrice.isFinite()) {
            studentPrice.toEuro()
        } else {
            "N/A"
        }

        if (normalPrice.isFinite()) {
            result += " / " + normalPrice.toEuro()
        }

        return result
    }

    /**
     * Compares this price to another.
     * * If both object's [studentPrice] is [Double.NaN], their [normalPrice] is compared according
     * to [Double.compareTo].
     * * Otherwise, both object's [studentPrice] is compared according to [Double.compareTo]
     */
    override fun compareTo(other: Price) =
        if (studentPrice.isNaN() && other.studentPrice.isNaN()) {
            normalPrice.compareTo(other.normalPrice)
        } else {
            studentPrice.compareTo(other.studentPrice)
        }

    private fun Double.toEuro(): String {
        return if (isFinite()) {
            val intermediate = round(2)
            var result = intermediate.toString()
            if (intermediate == intermediate.round(1)) {
                result += "0"
            }
            "$result €"
        } else {
            ""
        }
    }
}
