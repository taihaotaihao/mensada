/*
 * MIT License
 *
 * Copyright (c) 2022-2022.
 * Taihao Zhang (http://taihao.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.taihao.mensada.model

import java.time.DayOfWeek
import java.util.function.BiFunction

/**
 * A map containing an entire week's menus, mapping each day of the week to a menu.
 * The [canteen] field stores information about the specific canteen this weekly menu belongs to.
 * Attempting to add a day's menu of a different canteen to this map throws an exception.
 * @property canteen Canteen that this weekly menu belongs to
 */
data class WeeklyMenu(val canteen: Canteen) : HashMap<DayOfWeek, Menu>() {
    override fun put(key: DayOfWeek, value: Menu): Menu? {
        if (value.canteen == canteen) {
            return super.put(key, value)
        } else {
            throw IllegalStateException("Mismatched canteen. Map expects $canteen")
        }
    }

    override fun putAll(from: Map<out DayOfWeek, Menu>) {
        if (from.all { it.value.canteen == canteen }) {
            return super.putAll(from)
        } else {
            throw IllegalStateException("Mismatched canteen. Map expects $canteen")
        }
    }

    override fun putIfAbsent(key: DayOfWeek, value: Menu): Menu? {
        if (value.canteen == canteen) {
            return super.put(key, value)
        } else {
            throw IllegalStateException("Mismatched canteen. Map expects $canteen")
        }
    }

    override fun replace(key: DayOfWeek, value: Menu): Menu? {
        if (value.canteen == canteen) {
            return super.put(key, value)
        } else {
            throw IllegalStateException("Mismatched canteen. Map expects $canteen")
        }
    }

    override fun replace(key: DayOfWeek, oldValue: Menu, newValue: Menu): Boolean {
        if (newValue.canteen == canteen) {
            return super.replace(key, oldValue, newValue)
        } else {
            throw IllegalStateException("Mismatched canteen. Map expects $canteen")
        }
    }

    override fun replaceAll(function: BiFunction<in DayOfWeek, in Menu, out Menu>) {
        if (all { function.apply(it.key, it.value).canteen == canteen }) {
            return super.replaceAll(function)
        } else {
            throw IllegalStateException("Mismatched canteen. Map expects $canteen")
        }
    }

    override fun toString(): String {
        // Needed so toString prints the elements of this collection
        return super.toString()
    }
}
