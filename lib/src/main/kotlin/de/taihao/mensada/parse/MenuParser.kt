/*
 * MIT License
 *
 * Copyright (c) 2022-2022.
 * Taihao Zhang (http://taihao.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.taihao.mensada.parse

import de.taihao.mensada.model.* // ktlint-disable no-wildcard-imports
import org.jsoup.nodes.Element
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * Parses an entire week's menu from an HTML element.
 *
 * ```html
 * <div class="fmc-main">
 *     <section class="fmc-day">
 *         .
 *         .
 *         .
 * ```
 */
internal fun Element.parseToWeeklyMenu(canteen: Canteen): WeeklyMenu {
    val menus = getElementsByClass("fmc-day")
        .associate {
            it.parseToDate().dayOfWeek to it.parseToMenu(canteen)
        }.toSortedMap()
    val wm = WeeklyMenu(canteen)
    wm.putAll(menus)
    return wm
}

/**
 * Parses an entire menu from an HTML element.
 *
 * ```html
 * <section class="fmc-day">
 *     <div class="fmc-head">Montag		<span class="light">02.05.2022</span></div>
 * </section>
 * ```
 *
 * @receiver HTML element containing a menu
 * @return parsed menu
 */
internal fun Element.parseToMenu(canteen: Canteen): Menu {
    // Parse the date from HTML
    val date = parseToDate()

    // Construct menu with date and alleged canteen
    val menu = Menu(date, canteen)

    // Add all parsed meals
    menu.addAll(
        getElementsByClass("fmc-body")[0]
            .child(0)
            .getElementsByClass("fmc-item")
            .map { it.parseToMeal() }
            .distinct()
    )

    return menu
}

/**
 * Parses a meal from an HTML element.
 * ```html
 <li class="fmc-item" data-cat="2">
 <span class="fmc-item-icon">...</span>
 <span class="fmc-item-title">Gnocchi-Pfanne mit Mais, Zuckerschoten und Gemüse-Julienne (A,I)</span>
 <span class="fmc-item-location">Bistro</span>
 <span class="fmc-item-price">2,30  €</span>
 </li>
 ```
 * @receiver HTML element containing a meal
 * @return parsed meal
 */
internal fun Element.parseToMeal(): Meal {
    val (title, allergens) = getElementsByClass("fmc-item-title")[0].text().trim()
        .splitIntoTitleAndAllergens()
    val price = getElementsByClass("fmc-item-price")[0].text().parseToPrice()
    val loc = getElementsByClass("fmc-item-location")[0].text().trim()
    return Meal(title, allergens, attr("data-cat").toInt(), price, loc)
}

/**
 * Parses a price string of the form "2.50 € 3.50 €" and assigns the first price
 * to be the [Price.studentPrice], and the second one to be the [Price.normalPrice].
 * @receiver price string
 * @return parsed price
 */
internal fun String.parseToPrice(): Price {
    if (isBlank()) return Price(Double.NaN, Double.NaN)
    val prices = mutableListOf<Double>()
    with(Regex("""(?i)(\d+([.,]\d+)?) *(€|(euro?))""").toPattern().matcher(this)) {
        while (find()) {
            prices.add(group(1).replace(",", ".").toDouble())
        }
    }

    return when (prices.size) {
        0 -> Price(Double.NaN, Double.NaN)
        1 -> Price(prices[0], Double.NaN)
        else -> Price(prices[0], prices[1])
    }
}

/**
 * fmc-day
 */
private fun Element.parseToDate() = LocalDate.parse(
    getElementsByClass("fmc-head")[0].getElementsByClass("light").text().trim(),
    DateTimeFormatter.ofPattern("dd.MM.yyyy")
)

private fun String.splitIntoTitleAndAllergens(): Pair<String, String> {
    // Examples of allergens this regex matches
    // (2,3,C,G) (A)
    // (1,9,A,J,C,G)
    val matcher = """(\((([A-N0-9]|10)(, *)?)+\) *)+""".toPattern().matcher(this)
    return if (matcher.find()) {
        val match = matcher.group()
        replace(match, "").trim() to match.trim()
    } else {
        trim() to ""
    }
}
