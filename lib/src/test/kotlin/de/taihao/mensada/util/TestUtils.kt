/*
 * MIT License
 *
 * Copyright (c) 2022-2022.
 * Taihao Zhang (http://taihao.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.taihao.mensada.util

import de.taihao.mensada.model.Canteen
import de.taihao.mensada.model.Meal
import de.taihao.mensada.model.Menu
import de.taihao.mensada.model.Price
import java.time.LocalDate
import kotlin.random.Random

internal fun randomMeal(): Meal {
    val names = listOf("Spaghetti", "Schnitzel", "Knödel", "Nudeln", "Suppe").random()
    val allergens = listOf("(A)", "(A,B,3)", "(G,H,J,2,3)").random()
    val price = Price(Random.nextDouble() * 3, Random.nextDouble() * 3 + 3)
    val location = listOf("Marktrestaurant", "Otto-Berndt-Halle", "Bistro", "Gabel").random()

    return Meal(names, allergens, (1..14).random(), price, location)
}

internal fun randomMenu(): Menu {
    val menu = Menu(LocalDate.now(), Canteen.values().random())
    repeat((9..14).random()) {
        menu.add(randomMeal())
    }
    return menu
}
