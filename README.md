# Mensa Darmstadt
<a href='https://jitpack.io/#com.gitlab.taihaotaihao/mensada/'><img src='https://jitpack.io/v/com.gitlab.taihaotaihao/mensada.svg'/></a>

Kotlin library to query daily menus of the canteens of Studierendenwerk Darmstadt (Technische Universität Darmstadt, Hochschule Darmstadt).

Example usage:
```kotlin
val weeklyMenu = MenuSupplier.getWeeklyMenu(Canteen.STADTMITTE)
val mondayMenu = weeklyMenu[DayOfWeek.MONDAY]
val spaghettiMeal = mondayMenu.first { it.title == "Spaghetti" }
```

This library can query the five canteen locations that the Studierendenwerk mentions [at this link](https://studierendenwerkdarmstadt.de/hochschulgastronomie/speisen/).
* Stadtmitte
* Lichtwiese
* Schöfferstraße
* Dieburg
* Schöffers
